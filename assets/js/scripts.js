var days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat']
var daysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

var weather = {Clear: 'fas fa-sun', Rain: 'fas fa-cloud-rain', Clouds: 'fas fa-cloud', Thunderstorm: 'fas fa-bolt'};
var weatherStyle = {Clear: 'style="font-size:3rem;color:gold"', Rain: 'style="font-size:3rem;color:blue"', Clouds: 'style="font-size:3rem;color:gray"', Thunderstorm: 'style="font-size:3rem;color:orange"'}

var weatherStyleLG = {Clear: 'style="font-size:8rem;color:gold"', Rain: 'style="font-size:8rem;color:blue"', Clouds: 'style="font-size:8rem;color:gray"', Thunderstorm: 'style="font-size:8rem;color:orange"'}

// Format day and time
var day = new Date();
var currDay = day.getDay();
var hour = day.getHours();
var minutes = day.getMinutes();
var ampm = undefined;
if (parseInt(hour) < 12) {
    ampm ='am';
} else {
    hour = hour - 12;
    ampm = 'pm'
};
if (minutes < 10) {
    minutes = '0'+ minutes.toString();
}
var currTime = hour + ':' + minutes + ampm;

$(document).ready(function() {
    // Default to Pittsburgh
    getWeatherData('40.8471', '-73.9243');

    // Search functionality
    document.getElementById('search-btn').addEventListener('click', function(event) {
        event.preventDefault();
        var locString = $.find('input[name=loc]')[0].value.split('&');

        getWeatherData(locString[0], locString[1]);
    });


});

function getWeatherData(lat, long) {
    // Write a function to use the DarkSky api to gather weather data and update the screen.

    var req = 'https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+long+'&exclude=minutely,hourly&units=imperial&appid=28badde32e2d8fc0f70526716f831082'

    $.get(req, function(data) {
        // Today general
        $('#today-icon').html(function() {
            var main = data.current.weather[0].main;
            var day = '<i class="'+ weather[main] + '"' + weatherStyleLG[main] + '></i>';
            return day;
        });
        $('#today-temp').html(function() {
            return Math.round(data.current.temp) + '°F';
        });
        $('#day-time').html(function() {
            return  daysFull[currDay]+ ', ' + currTime;
        });

        // Forecast
        $('#day-1').html(function() {
            // where x is the day of the week and location in weather array
            var x = 0;
            var main = data.daily[x].weather[0].main;
            var day = '<p id="week-day">' + days[currDay] + '</p>' + '<i id="week-icon" class="'+ weather[main] + '"' + weatherStyle[main] + '></i> <p id="week-temp">' + Math.round(data.daily[x].temp.max) + ' / ' + Math.round(data.daily[x].temp.min)+'</p>';

            return '<div id="day">'+day+'</div>';

        });
        $('#day-2').html(function() {
            var x = 1;
            var main = data.daily[x].weather[0].main;
            var newDay = currDay + 1;
            var day = '<p id="week-day">' + days[newDay] + '</p>' + '<i id="week-icon" class="'+ weather[main] + '"' + weatherStyle[main] + '></i> <p id="week-temp">' + Math.round(data.daily[x].temp.max) + ' / ' + Math.round(data.daily[x].temp.min)+'</p>';

            return '<div id="day">'+day+'</div>';
        });
        $('#day-3').html(function() {
            var x = 2;
            var newDay = currDay + 2;
            var main = data.daily[x].weather[0].main;
            var day = '<p id="week-day">' + days[newDay] + '</p>' + '<i id="week-icon" class="'+ weather[main] + '"' + weatherStyle[main] + '></i> <p id="week-temp">' + Math.round(data.daily[x].temp.max) + ' / ' + Math.round(data.daily[x].temp.min)+'</p>';

            return '<div id="day">'+day+'</div>';
        });
        $('#day-4').html(function() {
            var x = 3;
            var newDay = currDay + 3;
            var main = data.daily[x].weather[0].main;
            var day = '<p id="week-day">' + days[newDay] + '</p>' + '<i id="week-icon" class="'+ weather[main] + '"' + weatherStyle[main] + '></i> <p id="week-temp">' + Math.round(data.daily[x].temp.max) + ' / ' + Math.round(data.daily[x].temp.min)+'</p>';

            return '<div id="day">'+day+'</div>';
        });
        $('#day-5').html(function() {
            var x = 4;
            var newDay = currDay + 4;
            var main = data.daily[x].weather[0].main;
            var day = '<p id="week-day">' + days[newDay] + '</p>' + '<i id="week-icon" class="'+ weather[main] + '"' + weatherStyle[main] + '></i> <p id="week-temp">' + Math.round(data.daily[x].temp.max) + ' / ' + Math.round(data.daily[x].temp.min)+'</p>';

            return '<div id="day">'+day+'</div>';
        });
        $('#day-6').html(function() {
            var x = 5;
            var newDay = currDay + 5;
            var main = data.daily[x].weather[0].main;
            var day = '<p id="week-day">' + days[newDay] + '</p>' + '<i id="week-icon" class="'+ weather[main] + '"' + weatherStyle[main] + '></i> <p id="week-temp">' + Math.round(data.daily[x].temp.max) + ' / ' + Math.round(data.daily[x].temp.min)+'</p>';

            return '<div id="day">'+day+'</div>';
        });
        $('#day-7').html(function() {
            var x = 6;
            var newDay = currDay + 6;
            var main = data.daily[x].weather[0].main;
            var day = '<p id="week-day">' + days[newDay] + '</p>' + '<i id="week-icon" class="'+ weather[main] + '"' + weatherStyle[main] + '></i> <p id="week-temp">' + Math.round(data.daily[x].temp.max) + ' / ' + Math.round(data.daily[x].temp.min)+'</p>';

            return '<div id="day">'+day+'</div>';
        });

        // Today Highlights
        $('#uv').html(function() {
            var uv = '<div class="row"><div id="highlight" class="col">UV Index</div></div><div class="row"><div id="highlight-data" class="col">'+ data.current.uvi + '</div></div>'

            return '<div id="data">'+uv+'</div>';
        });
        $('#wind').html(function() {
            var wind = '<div class="row"><div id="highlight" class="col">Wind Status</div></div><div class="row"><div id="highlight-data" class="col">'+ data.current.wind_speed + ' mph</div></div>'

            return '<div id="data">'+wind+'</div>';
        });
        $('#sun').html(function() {
            var rise = new Date(data.current.sunrise);
            var sunrise = rise.toTimeString().split(':');
            sunrise = parseInt(sunrise[0])+':'+sunrise[1];

            var set = new Date(data.current.sunset);
            var sunset = set.toTimeString().split(':');
            sunset = parseInt(sunset[0])+':'+sunset[1];

            var sun = '<div class="row"><div id="highlight" class="col-12">Sunrise & Sunset</div></div><div class="row"><div id="highlight-sun" class="col-12"><i class="fas fa-arrow-circle-up" style="color: gold"></i>'+ sunrise + ' AM</div><div id="highlight-sun" class="col-12"><i class="fas fa-arrow-circle-down" style="color: gold"></i>'+ sunset + ' PM</div></div>'

            return '<div id="data">'+sun+'</div>';
        });
        $('#humidity').html(function() {
            var humidity = '<div class="row"><div id="highlight" class="col">Humidity</div></div><div class="row"><div id="highlight-data" class="col">'+ data.current.humidity + '%</div></div>'

            return '<div id="data">'+humidity+'</div>';
        });
        $('#visibility').html(function() {
            var visibility = '<div class="row"><div id="highlight" class="col">Visibility</div></div><div class="row"><div id="highlight-data" class="col">'+ parseFloat(data.current.visibility*0.00062137).toFixed(2) + ' mi</div></div>'

            return '<div id="data">'+visibility+'</div>';

        });
        $('#dew').html(function() {
            var dew = '<div class="row"><div id="highlight" class="col">Dew Point</div></div><div class="row"><div id="highlight-data" class="col">'+ Math.floor(data.current.dew_point) + '%</div></div>'

            return '<div id="data">'+dew+'</div>';;
        });
    })

    // Optionally, if you would rather use a library as was discussed in the README, feel free to do so.
}


