# TreatWeather - Front-end Engineer Evaluation

## Reverse Engineer Results

The application initially loads with Pittsburgh, PA lattitude and longitude. Upon searching, the weather updates. The lattitude and longitude should be entered in this format: 37&122



### Summary of initialization:

```shell
git clone https://gitlab.com/treatspace/developer/front-end-engineer-evaluation.git
cd front-end-engineer-evaluation
npm install
gulp
```

After running the initialization commands, open the index.HTML file in the browser to see the application.


![Desktop TreatWeather](assets/img/Desktop.png "Desktop TreatWeather")

![Mobile Top TreatWeather](assets/img/MobileTop.png "Mobile Top TreatWeather")

![Mobile Bottom TreatWeather](assets/img/MobileBottom.png "Mobile Bottom TreatWeather")

## Reverse Engineer Objective:

<img src="https://gitlab.com/treatspace/developer/front-end-engineer-evaluation/raw/master/assets/img/FinishedDesign.png" />


This project is designed to be a simple Front-end Engineer evaluation to allow us to get a first hand look at how job candidates think and approach a problem. As a company, we feel like whiteboarding interviews don't truly test an engineer on their abilities that they use on a day to day basis, but rather test their ability to solve HackerRank style questions.

Our goal during our interview process is to have a conversation with you to see how you think and approach problems, learn your interests, and identify your strong suits. We don't think interviews should be an interrogation or a test, therefore, we don't use automated coding tests or whiteboard sessions during our process.

Instead, we put together this simple evaluation process that will have you do a few small things that do happen on a very regular basis here at Treatspace and in most front-end engineering jobs. Below you will find the simple installation process as well as details as to what we would like you to do.

## Installing / Getting Started

A quick introduction of the minimial setup you need to get this application up and running.

Fork a copy of the project into your own repo, then:

```shell
git clone https://gitlab.com/treatspace/developer/front-end-engineer-evaluation.git
cd front-end-engineer-evaluation
npm install
```

Once you have all of the packages installed, remain in the root directory of the project and run `gulp`
This will start the processes in the gulpfile to compile, minify, and compress our SCSS/JS files. From there all you need to do is open index.html and you are free to begin coding!

## Developing
The idea behind this project is to demonstrate 2 very basic skills that you would encounter on a daily basis here at Treatspace:

1. Being able to create a page with a certain style based off of screenshots

2. Being able to pull data from a GET request and update the frontend based on the data you receive.

For this project, I would like you to use the OpenWeather API (https://openweathermap.org/api/one-call-api) and pull in the current weather conditions & 5 days of weather forecast for a given location.



### OpenWeather API
You will need to register for a free api key from the OpenWeather site to be used in your local testing, but when submitting your code, feel free to remove that as I will use our own test key when running your code. Another thing to note about this API is that in order to pull forecast data, it is looking for lat/long values. In a real world environment, we wouldn't expect users to pass a lat/long string to get their forecast, but to keep this evaluation short, you don't have to worry about setting anything up to convert an address to lat/long.

In order to save time, you also don't have to code the functionality for switching between celsius and fahrenheit, but the icons themselves should be included.


### Things we'll be looking at
Once you submit you merge request, these are some of the things we will be taking a look at to see how you approached the problem:

* Does the UI match the screenshots
* Does the UI scale well across multiple screen sizes? (We have provided a desktop sized design, but how it translates to mobile screens is up to you)
* How did you approach the API & update the UI with the data?

We understand that many people already have full time jobs as well as a home life, so we only want you to spend 3-4 hours MAX working on this problem. If you can finish it quicker than that, that is perfectly fine, but don't worry about spending long hours working on this. This evaluation is simply to see how you approach a problem and allow us to have a conversation around your approach.



## When you are finished
Once you've completed your code, email Chad (chad.moyer@treatspace.com) with a link to your repo to fork and review your code. If you have any questions regarding the project or if something was unclear, feel free to reach out and we'll get back to you as soon as we can!

We'd like you to finish this evaluation before Tuesday (Oct 27nd, 2020). If you are not able to, let us know and we can discuss more.

We look forward to talking with you!





